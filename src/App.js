import logo from "./logo.svg";
import "./App.css";
import Shoe_Shop from "./Ex_ShoeShop/Shoe_Shop";

function App() {
  return (
    <div className="App">
      <Shoe_Shop />
    </div>
  );
}

export default App;

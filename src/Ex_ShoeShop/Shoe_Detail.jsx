import React, { Component } from 'react'

export default class Shoe_Detail extends Component {
    render() {
        let { image, name, price, description } = this.props.shoeDetail
        return (
            <div className='container p-5'>
                <div className="row">
                    <div className="col-4">
                        <img src={image} alt="" className='w-100' />
                    </div>
                    <div className="col-8">
                        <p>Ten: {name}</p>
                        <p>Price: {price}</p>
                        <p>Mo Ta: {description}</p>
                    </div>
                </div>
            </div>
        )
    }
}

import React, { Component } from 'react'
import { dataShoe } from './data_shoe'
import Shoe_Cart from './Shoe_Cart'
import Shoe_Detail from './Shoe_Detail'
import Shoe_List from './Shoe_List'



export default class Shoe_Shop extends Component {
    state = {
        shoeList: dataShoe,
        shoeDetail: dataShoe[0],
        shoeCart: [],
    }

    handleShowDetail = (shoeId) => {
        let index = this.state.shoeList.findIndex((item) => {
            return item.id == shoeId
        })
        index !== -1 && this.setState({
            shoeDetail: dataShoe[index]
        })
    };

    handleAddToCart = (shoe) => {
        let cloneShoeCart = [...this.state.shoeCart]
        let index = this.state.shoeCart.findIndex((item) => {
            return item.id == shoe.id
        })
        if (index == -1) {
            let cartItem = { ...shoe, soLuong: 1 }
            cloneShoeCart.push(cartItem)
        } else {
            cloneShoeCart[index].soLuong++;
            console.log(cloneShoeCart[index].soLuong)
        }
        this.setState({
            shoeCart: cloneShoeCart,
        })
    }

    handleDeleteFromCart = (shoe) => {
        let cloneShoeCart = [...this.state.shoeCart]
        let index = this.state.shoeCart.findIndex((item) => {
            return item.id == shoe.id
        })

        if (index !== -1) {
            cloneShoeCart.splice(index - 1, 1)
        }
        this.setState({
            shoeCart: cloneShoeCart,
        })
    }

    handleIncreaseQuantity = (shoe) => {
        let cloneShoeCart = [...this.state.shoeCart]
        let index = this.state.shoeCart.findIndex((item) => {
            return item.id == shoe.id
        })
        cloneShoeCart[index].soLuong++;
        this.setState({
            shoeCart: cloneShoeCart,
        })
    }

    handleDecreaseQuantity = (shoe) => {
        let cloneShoeCart = [...this.state.shoeCart]
        let index = this.state.shoeCart.findIndex((item) => {
            return item.id == shoe.id
        })
        cloneShoeCart[index].soLuong--;
        this.setState({
            shoeCart: cloneShoeCart,
        })
    }


    render() {
        return (
            <div>
                <Shoe_Cart
                    handleDecreaseQuantity={this.handleDecreaseQuantity}
                    handleIncreaseQuantity={this.handleIncreaseQuantity}
                    handleDeleteFromCart={this.handleDeleteFromCart}
                    shoeCart={this.state.shoeCart} />
                <Shoe_List
                    handleAddToCart={this.handleAddToCart}
                    handleShowDetail={this.handleShowDetail}
                    data={this.state.shoeList} />
                <Shoe_Detail shoeDetail={this.state.shoeDetail} />
            </div>
        )
    }
}

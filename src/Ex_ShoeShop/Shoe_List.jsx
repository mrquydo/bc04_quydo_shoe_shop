import React, { Component } from 'react'
import Shoe_Item from './Shoe_Item'

export default class Shoe_List extends Component {
    render() {
        return (
            <div className='container'>
                <div className="row">
                    {this.props.data.map((item, index) => {
                        return (
                            <div key={index} className="col-3">
                                <Shoe_Item
                                    handleAddToCart={this.props.handleAddToCart}
                                    handleShowDetail={this.props.handleShowDetail}
                                    item={item} />
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    }
}

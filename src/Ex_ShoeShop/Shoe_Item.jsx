import React, { Component } from 'react'

export default class Shoe_Item extends Component {
    render() {
        let { name, image, shortDescription } = this.props.item;
        return (
            <div className="card" style={{ width: '18rem' }}>
                <img className="card-img-top" src={image} alt="Card image cap" />
                <div className="card-body">
                    <h5 className="card-title">{name}</h5>
                    <p className="card-text">{shortDescription}</p>
                    <button onClick={() => {
                        this.props.handleAddToCart(this.props.item)
                    }} className='btn btn-primary'>Add to cart</button>
                    <button onClick={() => {
                        this.props.handleShowDetail(this.props.item.id)
                    }} className='btn btn-success'>Show Details</button>
                </div>
            </div>
        )
    }
}

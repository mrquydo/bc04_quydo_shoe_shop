import React, { Component } from 'react'

export default class Shoe_Cart extends Component {
    renderTbody = () => {
        return this.props.shoeCart.map((item) => {
            if (this.props.shoeCart.length == 0) {
                return <p>Vui long them san pham</p>
            }
            return (
                <tr>
                    <td>{item.name}</td>
                    <td>{item.price}</td>
                    <td>
                        {""}
                        <img src={item.image} style={{ width: 80 }} alt="" />
                    </td>
                    <td>
                        <button onClick={() => {
                            this.props.handleDecreaseQuantity(item)
                        }} className='btn btn-warning'>-</button>
                        <span className='mx-3'>{item.soLuong}</span>
                        <button onClick={() => {
                            this.props.handleIncreaseQuantity(item)
                        }} className='btn btn-danger'>+</button>
                    </td>
                    <td>
                        <button onClick={() => { this.props.handleDeleteFromCart(item) }} className='btn btn-danger'>Delete</button>
                    </td>
                </tr>
            )
        })
    }
    render() {
        return (
            <div className="container py-5">
                <table className='table table-dark'>
                    <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Price</th>
                            <th scope="col">Image</th>
                            <th scope="col">Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderTbody()}
                    </tbody>
                </table>
            </div>
        )
    }
}
